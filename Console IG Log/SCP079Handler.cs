﻿using Smod2;
using Smod2.API;
using Smod2.EventHandlers;
using Smod2.Events;

namespace Console_IG_Log
{
    internal class SCP079Handler : IEventHandler079AddExp, IEventHandler079CameraTeleport, IEventHandler079Door, IEventHandler079Elevator, IEventHandler079ElevatorTeleport, IEventHandler079LevelUp, IEventHandler079Lock, IEventHandler079Lockdown, IEventHandler079StartSpeaker, IEventHandler079StopSpeaker, IEventHandler079TeslaGate, IEventHandler079UnlockDoors, IEventHandlerGeneratorAccess, IEventHandlerGeneratorEjectTablet, IEventHandlerGeneratorFinish, IEventHandlerGeneratorInsertTablet, IEventHandlerGeneratorUnlock
    {
        private ConsoleIG consoleIG;
        private IConfigFile cf = ConfigManager.Manager.Config;
        private Server server = PluginManager.Manager.Server;

        public SCP079Handler(ConsoleIG consoleIG)
        {
            this.consoleIG = consoleIG;
        }

        public void On079AddExp(Player079AddExpEvent ev)
        {
            if(cf.GetBoolValue("lig_log_079_addexp", false))
            {
                string msg = consoleIG.GetTranslation("on079AddExp");
                msg = msg.Replace("[player_name]", ev.Player.Name);
                msg = msg.Replace("[amount_xp]", ev.ExpToAdd.ToString());
                msg = msg.Replace("[type]", ev.ExperienceType.ToString());
                foreach (Player p in server.GetPlayers())
                {
                    if (perm.CheckPerm(p))
                    {
                        p.SendConsoleMessage(msg);
                    }
                }
            }
        }

        public void On079CameraTeleport(Player079CameraTeleportEvent ev)
        {
            if (cf.GetBoolValue("lig_log_079_camerateleport", false))
            {
                string msg = consoleIG.GetTranslation("on079CameraTeleport");
                msg = msg.Replace("[player_name]", ev.Player.Name);

                foreach (Player p in server.GetPlayers())
                {
                    if (perm.CheckPerm(p))
                    {
                        p.SendConsoleMessage(msg);
                    }
                }
            }
        }

        public void On079Door(Player079DoorEvent ev)
        {
            if (cf.GetBoolValue("lig_log_079_door", false))
            {
                
                if(ev.Door.Open == true)
                {
                    string msg = consoleIG.GetTranslation("on079DoorClose");
                    msg = msg.Replace("[player_name]", ev.Player.Name);
                    msg = msg.Replace("[door_name]", ev.Door.Name);
                    foreach (Player p in server.GetPlayers())
                    {
                        if (perm.CheckPerm(p))
                        {
                            p.SendConsoleMessage(msg, "blue");
                        }
                    }
                }
                else
                {
                    string msg = consoleIG.GetTranslation("on079DoorOpen");
                    msg = msg.Replace("[player_name]", ev.Player.Name);
                    msg = msg.Replace("[door_name]", ev.Door.Name);
                    foreach (Player p in server.GetPlayers())
                    {
                        if (perm.CheckPerm(p))
                        {
                            p.SendConsoleMessage(msg, "blue");
                        }
                    }
                }
                
            }
        }

        public void On079Elevator(Player079ElevatorEvent ev)
        {
            if (cf.GetBoolValue("lig_log_079_addexp", false))
            {
                string msg = consoleIG.GetTranslation("on079Elevator");
                msg = msg.Replace("[player_name]", ev.Player.Name);
                foreach (Player p in server.GetPlayers())
                {
                    if (perm.CheckPerm(p))
                    {
                        p.SendConsoleMessage(msg, "blue");
                    }
                }
            }
        }

        public void On079ElevatorTeleport(Player079ElevatorTeleportEvent ev)
        {
            if (cf.GetBoolValue("lig_log_079_elevator", false))
            {
                string msg = consoleIG.GetTranslation("on079ElevatorTeleport");
                msg = msg.Replace("[player_name]", ev.Player.Name);
                foreach (Player p in server.GetPlayers())
                {
                    if (perm.CheckPerm(p))
                    {
                        p.SendConsoleMessage(msg);
                    }
                }
            }
        }

        public void On079LevelUp(Player079LevelUpEvent ev)
        {
            if (cf.GetBoolValue("lig_log_079_levelup", false))
            {
                string msg = consoleIG.GetTranslation("on079LevelUp");
                msg = msg.Replace("[player_name]", ev.Player.Name);
                foreach (Player p in server.GetPlayers())
                {
                    if (perm.CheckPerm(p))
                    {
                        p.SendConsoleMessage(msg);
                    }
                }
            }
        }

        public void On079Lock(Player079LockEvent ev)
        {
            if (cf.GetBoolValue("lig_log_079_addexp", false))
            {
                string msg = consoleIG.GetTranslation("on079Lock");
                msg = msg.Replace("[player_name]", ev.Player.Name);
                msg = msg.Replace("[door_name]", ev.Door.Name);
                foreach (Player p in server.GetPlayers())
                {
                    if (perm.CheckPerm(p))
                    {
                        p.SendConsoleMessage(msg, "blue");
                    }
                }
            }
        }

        public void On079Lockdown(Player079LockdownEvent ev)
        {
            if (cf.GetBoolValue("lig_log_079_addexp", false))
            {
                string msg = consoleIG.GetTranslation("on079Lockdown");
                msg = msg.Replace("[player_name]", ev.Player.Name);
                msg = msg.Replace("[zone]", ev.Room.ZoneType.ToString());
                foreach (Player p in server.GetPlayers())
                {
                    if (perm.CheckPerm(p))
                    {
                        p.SendConsoleMessage(msg, "blue");
                    }
                }
            }
        }

        public void On079StartSpeaker(Player079StartSpeakerEvent ev)
        {
            if (cf.GetBoolValue("lig_log_079_addexp", false))
            {
                string msg = consoleIG.GetTranslation("on079StartSpeaker");
                msg = msg.Replace("[player_name]", ev.Player.Name);
                msg = msg.Replace("[zone]", ev.Room.ZoneType.ToString());

                foreach (Player p in server.GetPlayers())
                {
                    if (perm.CheckPerm(p))
                    {
                        p.SendConsoleMessage(msg);
                    }
                }
            }
        }

        public void On079StopSpeaker(Player079StopSpeakerEvent ev)
        {
            if (cf.GetBoolValue("lig_log_079_addexp", false))
            {
                string msg = consoleIG.GetTranslation("on079StopSpeaker");
                msg = msg.Replace("[player_name]", ev.Player.Name);
                msg = msg.Replace("[zone]", ev.Room.ZoneType.ToString());
                foreach (Player p in server.GetPlayers())
                {
                    if (perm.CheckPerm(p))
                    {
                        p.SendConsoleMessage(msg);
                    }
                }
            }
        }

        public void On079TeslaGate(Player079TeslaGateEvent ev)
        {
            if (cf.GetBoolValue("lig_log_079_addexp", false))
            {
                string msg = consoleIG.GetTranslation("on079TeslaGate");
                msg = msg.Replace("[player_name]", ev.Player.Name);
                foreach (Player p in server.GetPlayers())
                {
                    if (perm.CheckPerm(p))
                    {
                        p.SendConsoleMessage(msg, "yellow");
                    }
                }
            }
        }

        public void On079UnlockDoors(Player079UnlockDoorsEvent ev)
        {
            if (cf.GetBoolValue("lig_log_079_addexp", false))
            {
                string msg = consoleIG.GetTranslation("on079AddExp");
                msg = msg.Replace("[player_name]", ev.Player.Name);
                foreach (Player p in server.GetPlayers())
                {
                    if (perm.CheckPerm(p))
                    {
                        p.SendConsoleMessage(msg, "blue");
                    }
                }
            }
        }

        public void OnGeneratorAccess(PlayerGeneratorAccessEvent ev)
        {
            if (cf.GetBoolValue("lig_log_generator_access", true))
            {
                if(ev.Generator.Open == false)
                {
                    string msg = consoleIG.GetTranslation("onGeneratorAccessOpen");
                    msg = msg.Replace("[class_name]", consoleIG.GetTranslation(ev.Player.TeamRole.Role.ToString()));
                    msg = msg.Replace("[player_name]", ev.Player.Name);
                    foreach (Player p in server.GetPlayers())
                    {
                        if (perm.CheckPerm(p))
                        {
                            p.SendConsoleMessage(msg);
                        }
                    }
                }
                else
                {
                    string msg = consoleIG.GetTranslation("onGeneratorAccessClose");
                    msg = msg.Replace("[class_name]", consoleIG.GetTranslation(ev.Player.TeamRole.Role.ToString()));
                    msg = msg.Replace("[player_name]", ev.Player.Name);
                    foreach (Player p in server.GetPlayers())
                    {
                        if (perm.CheckPerm(p))
                        {
                            p.SendConsoleMessage(msg);
                        }
                    }
                }
               
            }
        }

        public void OnGeneratorEjectTablet(PlayerGeneratorEjectTabletEvent ev)
        {
            if (cf.GetBoolValue("lig_log_generator_ejecttablet", false))
            {
                string msg = consoleIG.GetTranslation("onGeneratorEjectTablet");
                msg = msg.Replace("[class_name]", consoleIG.GetTranslation(ev.Player.TeamRole.Role.ToString()));
                msg = msg.Replace("[player_name]", ev.Player.Name);
                foreach (Player p in server.GetPlayers())
                {
                    if (perm.CheckPerm(p))
                    {
                        p.SendConsoleMessage(msg, "red");
                    }
                }
            }
        }

        public void OnGeneratorFinish(GeneratorFinishEvent ev)
        {
            if (cf.GetBoolValue("lig_log_generator_finish", false))
            {
                string msg = consoleIG.GetTranslation("onGeneratorFinish");
                foreach (Player p in server.GetPlayers())
                {
                    if (perm.CheckPerm(p))
                    {
                        p.SendConsoleMessage(msg, "red");
                    }
                }
            }
        }

        public void OnGeneratorInsertTablet(PlayerGeneratorInsertTabletEvent ev)
        {
            if (cf.GetBoolValue("lig_log_generator_inserttablet", false))
            {
                string msg = consoleIG.GetTranslation("onGeneratorEjectTablet");
                msg = msg.Replace("[class_name]", consoleIG.GetTranslation(ev.Player.TeamRole.Role.ToString()));
                msg = msg.Replace("[player_name]", ev.Player.Name);
                msg = msg.Replace("[time]", ev.Generator.TimeLeft.ToString());
                foreach (Player p in server.GetPlayers())
                {
                    if (perm.CheckPerm(p))
                    {
                        p.SendConsoleMessage(msg);
                    }
                }
            }
        }

        public void OnGeneratorUnlock(PlayerGeneratorUnlockEvent ev)
        {
            if (cf.GetBoolValue("lig_log_generator_unlock", false))
            {
                string msg = consoleIG.GetTranslation("onGeneratorUnlock");
                msg = msg.Replace("[class_name]", consoleIG.GetTranslation(ev.Player.TeamRole.Role.ToString()));
                msg = msg.Replace("[player_name]", ev.Player.Name);
                foreach (Player p in server.GetPlayers())
                {
                    if (perm.CheckPerm(p))
                    {
                        p.SendConsoleMessage(msg);
                    }
                }
            }
        }
    }
}