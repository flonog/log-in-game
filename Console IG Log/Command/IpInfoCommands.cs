﻿using Console_IG_Log;
using Smod2.API;
using Smod2.Commands;

namespace Command
{
    internal class IpInfoCommands : ICommandHandler
    {
        private ConsoleIG consoleIG;

        public IpInfoCommands(ConsoleIG consoleIG)
        {
            this.consoleIG = consoleIG;
        }

        public string GetCommandDescription()
        {
            return "Give some informations about this IP address.";
        }

        public string GetUsage()
        {
            return "IPINFO / IPI <Player Name | IP>";
        }

        public string[] OnCall(ICommandSender sender, string[] args)
        {
            string arg = args[1];
            string trim = "::ffff:";
            Player p = PlayerName.GetPlayer(arg);

            

            if (PlayerName.GetPlayer(arg) != null)
            {
                string Ip = p.IpAddress;
                Ip = Ip.Trim(trim.ToCharArray());
                string[] country = ip.GetUserCountryByIp(Ip);
                return new string[] { "The " + p.Name + "'s ip adress is coming from : " + country[0] + " [" + country[1] + "]" };
            }
            else
            {
                arg = arg.Trim(trim.ToCharArray());
                string[] country = ip.GetUserCountryByIp(arg);
                return new string[] { "The ip adress is coming from : " + country[0] + " [" + country[1] + "]" };
            }
        }
    }
}